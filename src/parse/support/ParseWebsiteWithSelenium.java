package parse.support;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class ParseWebsiteWithSelenium {
	private String url;
	
	public ParseWebsiteWithSelenium() {
		this("https://www.google.com");
	}
	
	public ParseWebsiteWithSelenium(String url) {
		this.url = url;
	}
	
	public String getHTMLSource() {
		//declare the chrome driver from the local machine location
		String pathChromeDriver = null;
		String osname = System.getProperty("os.name").toLowerCase();
		
		if(osname.contains("linux")) {
			pathChromeDriver = "chromedriver/chromedriver_linux";
		} else if(osname.contains("win")) {
			pathChromeDriver = "chromedriver/chromedriver_win.exe";
		} else if(osname.contains("mac")) {
			pathChromeDriver = "chromedriver/chromedriver_mac";
		} else {
			pathChromeDriver = "chromedriver/"; // no support os
		}
		
		System.setProperty("webdriver.chrome.driver", pathChromeDriver);
		   
		//create object of chrome options
		ChromeOptions options = new ChromeOptions();
		
		options.addArguments("headless");
		options.addArguments("user-agent=Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36");
		
		//pass the options parameter in the Chrome driver declaration
		WebDriver driver = null;
		try {
			driver = new ChromeDriver(options);
		} catch (Exception e) {
			System.out.println("No support in your's OS");
			System.exit(1);
		}
		
		driver.get(url);

		// get html page source
		String html = driver.getPageSource();
		
		//Close the driver
		driver.close();
		
		return html;
	}
	
	public void setUrl(String url) {
		this.url = url;
	}
}
